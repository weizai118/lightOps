# lightOps

#### 项目介绍
一个轻量级的运维管理系统。

目前在系统中添加了SQL脚本管理，可以一键将SQL脚本更新到N个库中。

再也不用担心忘记哪SQL有更新，哪些没有更新了。

后续准备再加入URL监控、端口监控、磁盘监控等。

做这个的主要原因是因为，目前存在的zabbix、nagios等，安装部署都较为复杂，且基本只能部署到Linux服务器上，希望这个项目对大家有帮助。

该项目是在Jeecg-3.7.8版本的基本上开发的，代码也比较容易维护，方便二次开发。

如果有什么建议，可以给我留言。


Jeecg社区地址：http://www.jeecg.org/


#### 安装教程

项目需使用Tomcat8.5部署，JDK1.8
建库建本：docs/jeecg-3.7.8-oc.sql

#### 使用说明

批量更新SQL脚本使用说明：
1、系统监控->多数据源管理，配置数据源。
2、运维管理->数据库管理，配置数据库。（配置数据库时，“数据库标识”填写“多数据源管理”里面的“多数据源主键”）
3、SQL管理->添加SQL
4、SQL管理->执行任务，关联SQL需要执行的数据库，点击“执行”按钮即可。


#### 界面截图：
![多数据源管理界面](https://images.gitee.com/uploads/images/2018/0930/112945_413bc53f_367746.png "1.png")
![数据库管理界面](https://images.gitee.com/uploads/images/2018/0930/113025_9eaded47_367746.png "2.png")
![SQL管理界面](https://images.gitee.com/uploads/images/2018/0930/113129_0af3d915_367746.png "3.png")
![添加SQL界面](https://images.gitee.com/uploads/images/2018/0930/113437_23c09411_367746.png "4.png")
![添加SQL执行任务界面](https://images.gitee.com/uploads/images/2018/0930/113504_399539f3_367746.png "5.png")