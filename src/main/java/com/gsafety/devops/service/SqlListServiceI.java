package com.gsafety.devops.service;
import java.util.List;
import org.jeecgframework.core.common.service.CommonService;

import com.gsafety.devops.entity.SqlListEntity;
import com.gsafety.devops.entity.SqlTaskEntity;

import java.io.Serializable;

public interface SqlListServiceI extends CommonService{
 	public void delete(SqlListEntity entity) throws Exception;
	/**
	 * 添加一对多
	 * 
	 */
	public void addMain(SqlListEntity sqlList,
	
	List<SqlTaskEntity> sqlTaskList) throws Exception ;
	/**
	 * 修改一对多
	 */
	public void updateMain(SqlListEntity sqlList,
	List<SqlTaskEntity> sqlTaskList) throws Exception;
	public void delMain (SqlListEntity sqlList) throws Exception;
	/**
	 *行编辑新增
	 */
	public void addSqlListEntity(SqlListEntity sqlList) throws Exception;
	/**
	 *行编辑编辑
	 */
	public void updateSqlListEntity(SqlListEntity sqlList) throws Exception;
	
}
