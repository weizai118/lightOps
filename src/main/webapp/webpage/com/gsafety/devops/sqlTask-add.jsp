<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@include file="/context/mytags.jsp"%>
<html lang="zh-CN">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>执行任务</title>
  <meta name="description" content="">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <t:base type="jquery,aceform,DatePicker,validform,ueditor"></t:base>
  <style type="text/css">
  	.combo_self{height: 26px !important;width:164px !important;padding-top:0px !important;}
  	.layout-header .btn {
	    margin:0px;
	   float: none !important;
	}
	.btn-default {
	    height: 35px;
	    line-height: 35px;
	    font-size:14px;
	}
  </style>
  <script type="text/javascript">
	$(function(){
		$(".combo").removeClass("combo").addClass("combo combo_self");
		$(".combo").each(function(){
			$(this).parent().css("padding-top","10px !important;");
		});   
	});
  		
  		 /**树形列表数据转换**/
  function convertTreeData(rows, textField) {
      for(var i = 0; i < rows.length; i++) {
          var row = rows[i];
          row.text = row[textField];
          if(row.children) {
          	row.state = "open";
              convertTreeData(row.children, textField);
          }
      }
  }
  /**树形列表加入子元素**/
  function joinTreeChildren(arr1, arr2) {
      for(var i = 0; i < arr1.length; i++) {
          var row1 = arr1[i];
          for(var j = 0; j < arr2.length; j++) {
              if(row1.id == arr2[j].id) {
                  var children = arr2[j].children;
                  if(children) {
                      row1.children = children;
                  }
                  
              }
          }
      }
  }
  </script>
</head>
<body>
<t:formvalid formid="formobj" dialog="true" usePlugin="password" layout="table" action="sqlTaskController.do?doAdd" tiptype="1">
	<input type="hidden" id="btn_sub" class="btn_sub"/>
	<input type="hidden" name="id" value='${sqlTaskPage.id}' >
  	<input type="hidden" name="sqlId" value="${mainId}"/>
<div class="tab-wrapper">
	<!-- tab -->
	<ul class="nav nav-tabs">
		<li role="presentation" class="active"><a href="javascript:void(0);">执行任务</a></li>
	</ul>
	<!-- tab内容 -->	
	<div class="con-wrapper" id="con-wrapper1" style="display: block;">	
	<div class="row form-wrapper">
		<div class="row show-grid">
    	<div class="col-xs-3 text-center">
      		<b>数据库：</b>
        </div>
      	<div class="col-xs-3">
			<!-- <input name="dbId"  style="width:150px" maxlength="36" type="text" class="form-control"   datatype="n"  ignore="checked" /> -->
			<%-- <t:dictSelect field="dbId" type="checkbox"
									dictTable="database_info" dictField="id" dictText="db_desc" defaultVal="" hasLabel="false"  title="数据库" ></t:dictSelect> --%>
			
			<input id="dbIds" name="dbIds" type="hidden" value="" />
			<textarea name="dbDescs" class="inputxt" value="" id="dbDescs" readonly="readonly" datatype="*"  cols="40" rows="6"></textarea>
			<a href="#" class="easyui-linkbutton l-btn l-btn-plain" plain="true" id="departSearch" onclick="openSelectSort()">
					<span class="l-btn-text icon-search l-btn-icon-left">选择</span>
			</a>
			<a href="#" class="easyui-linkbutton l-btn l-btn-plain" plain="true" id="departRedo" onclick="callbackClean()">
				<span class="l-btn-text icon-redo l-btn-icon-left">清空</span>
			</a>
			
			<span class="Validform_checktip" style="float:left;height:0px;"></span>
			<label class="Validform_label" style="display: none">数据库</label>
        </div>
		</div>
		
		<%-- <input type="hidden" name="sqlId" value="${sqlId}"/> --%>
		<!-- <div class="row show-grid">
    	<div class="col-xs-3 text-center">
      		<b>SQL_ID：</b>
        </div>
      	<div class="col-xs-3">
			<input name="sqlId"  style="width:150px" maxlength="36" type="text" class="form-control"   datatype="*"  ignore="checked" />
			<span class="Validform_checktip" style="float:left;height:0px;"></span>
			<label class="Validform_label" style="display: none">SQL_ID</label>
        </div>
		</div> -->
		
		<input type="hidden" name="executeFlag" value="0"/>
		<%-- <div class="row show-grid">
    	<div class="col-xs-3 text-center">
      		<b>执行状态：</b>
        </div>
      	<div class="col-xs-3">
			<!-- <input name="executeFlag"  style="width:150px" maxlength="1" type="text" class="form-control"   datatype="n"  ignore="checked" /> -->
			<t:dictSelect field="executeFlag" type="select" typeGroupCode="exec_flag" hasLabel="false" title="执行状态" defaultVal="0"></t:dictSelect>
			<span class="Validform_checktip" style="float:left;height:0px;"></span>
			<label class="Validform_label" style="display: none">执行状态</label>
        </div>
		</div> --%>
		
            <div class="row" id = "sub_tr" style="display: none;">
	        <div class="col-xs-12 layout-header">
	          <div class="col-xs-6"></div>
	          <div class="col-xs-6"><button type="button" onclick="neibuClick();" class="btn btn-default">提交</button></div>
	        </div>
	      </div>
     </div>
   </div>
<div class="con-wrapper" id="con-wrapper2" style="display: block;"></div>
</div>
</t:formvalid>
<script type="text/javascript">
   $(function(){
    //查看模式情况下,删除和上传附件功能禁止使用
	if(location.href.indexOf("load=detail")!=-1){
		$(".jeecgDetail").hide();
	}
	
	if(location.href.indexOf("mode=read")!=-1){
		//查看模式控件禁用
		$("#formobj").find(":input").attr("disabled","disabled");
	}
	if(location.href.indexOf("mode=onbutton")!=-1){
		//其他模式显示提交按钮
		$("#sub_tr").show();
	}
   });

  var neibuClickFlag = false;
  function neibuClick() {
	  neibuClickFlag = true; 
	  $('#btn_sub').trigger('click');
  }

</script>


<script type="text/javascript">
function openSelectSort() {
	$.dialog.setting.zIndex = getzIndex(); 
	$.dialog({content: 'url:sqlTaskController.do?dbGridSelect', zIndex: getzIndex(), title: '数据库列表', lock: true, width: '700px', height: '400px', opacity: 0.4, button: [
		{name: '<t:mutiLang langKey="common.confirm"/>', callback: callbackRealNameSelect, focus: true},
		{name: '<t:mutiLang langKey="common.cancel"/>', callback: function (){}}
	]}).zindex();
}
function callbackRealNameSelect() {
	var iframe = this.iframe.contentWindow;
	var table = iframe.$("#table1");
	var id='',userName='';
	//$("#table1 tr :first-child").remove();
	$(table).find("tbody tr").each(function() {
		id += $(this).find("input").val()+",";
		userName += $(this).find("span").text()+",";
	})
	$("#dbDescs").val(userName);
	$("#dbDescs").blur();
	$("#dbIds").val(id);
}
function callbackClean(){
	$('#dbDescs').val('');
	$('#dbIds').val('');	
}
</script>

 </body>
</html>
